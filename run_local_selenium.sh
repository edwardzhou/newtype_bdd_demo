#!/bin/sh

HOUND_DRIVER=selenium
HOUND_HOST=192.168.216.102
HOUND_PORT=4444
HOUND_BROWSER=chrome

TEST_APP_HOST=localhost
TEST_APP_PORT=4001

docker-compose run \
  -e MIX_ENV=test \
  -e HOUND_DRIVER=$HOUND_DRIVER \
  -e HOUND_HOST=$HOUND_HOST \
  -e HOUND_PORT=$HOUND_PORT \
  -e HOUND_BROWSER=$HOUND_BROWSER \
  -e TEST_APP_HOST=$TEST_APP_HOST \
  -e TEST_APP_PORT=$TEST_APP_PORT \
  --name bdd_demo_app \
  --service-ports \
  --use-aliases \
  bdd_demo_app mix test

docker-compose down
