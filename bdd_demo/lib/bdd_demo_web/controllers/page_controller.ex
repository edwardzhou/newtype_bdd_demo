defmodule BddDemoWeb.PageController do
  use BddDemoWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
